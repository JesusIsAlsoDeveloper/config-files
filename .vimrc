set nocompatible              " be iMproved, requirn 'tpope/vim-fugitive'ed
filetype off                  " required


set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()
Plugin 'tpope/vim-fugitive'
Plugin 'tpope/vim-surround'
Plugin 'scrooloose/nerdtree'
Plugin 'valloric/youcompleteme'
Plugin 'scrooloose/syntastic'
Plugin 'mattn/emmet-vim'
Plugin 'honza/vim-snippets'
Plugin 'pangloss/vim-javascript'
Plugin 'christoomey/vim-tmux-navigator'



" All of your Plugins must be added before the following line
call vundle#end()            " required
filetype plugin indent on    " required

nmap <C-n> :NERDTreeToggle<CR>

" To ignore plugin indent changes, instead use:
"filetype plugin on
"
" Brief help
" :PluginList       - lists configured plugins
" :PluginInstall    - installs plugins; append `!` to update or just :PluginUpdate
" :PluginSearch foo - searches for foo; append `!` to refresh local cache
" :PluginClean      - confirms removal of unused plugins; append `!` to auto-approve removal
"
" see :h vundle for more details or wiki for FAQ
" Put your non-Plugin stuff after this line
